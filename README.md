# fast

This userscript makes the videos go fast.

## Installation

To add it to your browser, first ensure you have a userscript manager installed, like [TamperMonkey](https://www.tampermonkey.net/).


Then, [click here](https://gitlab.com/geeked/fast/-/raw/main/fast.user.js).



## Usage

Once the userscript is installed, reload any pages you want to use it on. Then, click the TamperMonkey icon, and fast > make fast.


![An image of the menu where you can click to make fast.](https://gitlab.com/geeked/fast/-/raw/main/fast.png)


You should now experience fast.


You may have to click it again if a video is dynamically added to the page. You can click it as much as you want.
