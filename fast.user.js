// ==UserScript==
// @name         fast
// @namespace
// @version      0.3
// @description  f a s t 
// @author       You
// @match        *://*/*
// @icon         https://panacea.sfo3.cdn.digitaloceanspaces.com/page/1eac2700-5758-4d96-b110-7b8d6b1f5cbb.jpeg
// @grant        GM_registerMenuCommand
// ==/UserScript==


(function() {
    'use strict';

    // set up the menu entry to activate fastness
    GM_registerMenuCommand ("make fast", main, "f", {autoClose: false});

    // set up the menu entry to activate super fastness. experimental indev
    GM_registerMenuCommand ("make unstoppably fast", unstoppablefast, "u", {autoClose: false});

    // clicks all the classes we know should be clicked
    function do_clicks(passed_document) {
        passed_document.querySelectorAll('.vjs-big-play-button, .cover__header-content-action-link, .continue-btn, .next-lesson__link, .block-flashcard__flip, .blocks-accordion__header, .labeled-graphic-marker, .flashcard, .process-arrow--right, .blocks-tabs__header-item').forEach(function(e){
            e.click()
        })
    }

    // speeds up all video elements in passed-in document
    function do_videos(passed_document) {
        let videos = passed_document.getElementsByTagName('video')
        console.log(videos)
        for(let i=0; i < videos.length; i++) {
            videos[i].playbackRate = 16
        }
    }

    // speeds up all video elements in passed-in document
    function do_audios(passed_document) {
        let audios = passed_document.getElementsByTagName('audio')
        console.log(audios)
        for(let i=0; i < audios.length; i++) {
            audios[i].playbackRate = 16
        }
    }

    // recursive document crawler
    function do_document(passed_document) {
        //fast any videos here
        do_videos(passed_document)
        //fast any audios here
        do_audios(passed_document)
        //click any clickables here
        do_clicks(passed_document)

        //get any iframes here
        let document_iframes = passed_document.querySelectorAll('iframe')
        console.log(document_iframes)
        // and recurse into their documents
        for(let i=0; i < document_iframes.length; i++) {
            let sub_document = document_iframes[i].contentWindow.document
            do_document(sub_document)
        }

    }

    // loop
    function unstoppablefast() {
        main()
        const myTimeout = setTimeout(unstoppablefast, 500);
    }

    // main
    function main() {

        do_document(document)

    }


})();